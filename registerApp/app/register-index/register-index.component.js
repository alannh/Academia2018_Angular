'use strict';

angular.
  module('registerApp').
  component('registerIndex', {
    templateUrl: './register-index/register-index.template.html',
    controller: ['User',
      function RegisterIndexController(User) {
        this.users = User.query();
      }
    ]
  });