'use strict';

angular.
  module('core.user').
  factory('User', ['$resource',
    function($resource) {
      return $resource('http://10.0.16.56:3000/users', {}, {
        query: {
          method: 'GET',
          params: {},
          isArray: true
        },
        'save':   {method:'POST'}
      });
    }
  ]);