'use strict';

angular.
  module('registerApp').
  component('registerIndex', {
    templateUrl: './register-index/register-index.template.html',
    controller: ['Item',
      function RegisterIndexController(Item) {
        this.users = Item.query();
        // this.user = Item.show(2);
        console.log(this.users);
        // this.orderProp = 'age';
      }
    ]
  });