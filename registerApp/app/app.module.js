'use strict';

angular.module('registerApp', [
  'ngRoute',
  'core',
  'registerIndex',
  'registerNewUser',
  'userDetails'
]);