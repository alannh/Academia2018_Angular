'use strict';

// Register `phoneDetail` component, along with its associated controller and template
angular.
  module('registerApp').
  component('userDetails', {    
    templateUrl: './user-details/user-details.template.html', //tempale:'TBD: Detail view for <span>{{$ctrl.userIS}}</span>'
    controller: ['$routeParams',
      function UserDetailsController(User) {
        //alert("holi3");
        //this.phoneId = $routeParams.userIS;
        this.users = User.query();
        this.userIS = 1;
      }
    ]
  });



/*
'use strict';

describe('myApp.view1 module', function() {

  beforeEach(module('myApp.view1'));

  describe('view1 controller', function(){

    it('should ....', inject(function($controller) {
      //spec body
      var view1Ctrl = $controller('View1Ctrl');
      expect(view1Ctrl).toBeDefined();
    }));

  });
});

*/

  