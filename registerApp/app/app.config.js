'use strict';

angular.
  module('registerApp').
  config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('');

      $routeProvider.
        when('/users', {
          template: '<register-index></register-index>'
        }).
        when('/users/new', {
          template: '<register-new-user></register-new-user>'
        }).
        when('/users/details', {
          template: '<user-details></user-details>'
        }).
        otherwise('/users');
    }
  ]);