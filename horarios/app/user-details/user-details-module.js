'use strict';

angular.module('horarios.user-details', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/user-details', {
    templateUrl: './user-details.html',
    controller: 'UserDetailsCtrl'
  });
}])

.controller('UserDetailsCtrl', [function() {

}]);